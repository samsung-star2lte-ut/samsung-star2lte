# Samsung Galaxy 9 Series Ubuntu Touch Port

This a Halium 10 based Ubuntu Touch port for Exynos 9810 devices

You can check the device progress checklist below.
* [Galaxy S9 (starlte)](-/blob/master/docs/DeviceChecklist-starlte.md)
* [Galaxy S9+ (star2lte)](-/blob/master/docs/DeviceChecklist-star2lte.md)
* [Galaxy Note 9 (crownlte)](-/blob/master/docs/DeviceChecklist-crownlte.md)

If you're interested in installing it on your device; 
* Download the latest "Devel-Flash" or "Flashable" from the [CI Pipelines](-/pipelines)
* Reboot to Recovery 
* Backup Boot+Data
* Back Up Files on Internal Storage
* Format Data 
* Copy `boot.img` and `ubuntu.img` to your device
* Flash `boot.img` to Boot
* Copy `ubuntu.img` to /data
* Reboot

If you're interested in contributing; you can do so by;
* Testing all areas (check the checklist)
* Reporting back any bugs or issues; **Logs will be incredibly useful**